package seguritech_cad_lite.integration_cad_lite_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrationCadLiteServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegrationCadLiteServiceApplication.class, args);
    }

}
